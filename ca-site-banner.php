<?php
/*
* Plugin Name: Creative Asset Site Banner
* Version: 1.0
* Author: Creative Asset
* Author URI: http://www.creative-asset.co.uk
*/

if ( ! defined( 'ABSPATH' ) ){
    exit;
}

class Creative_Asset_Site_Banner{

	function __construct(){
		add_action( 'wp_body_open', array($this, 'output_banner') );
		add_action('admin_menu', array($this,'ca_register_options_page'),99);
		add_action('admin_init', array($this,'ca_register_settings'));
		add_action('wp_enqueue_scripts', array($this, 'ca_scripts'));
        // add_action( 'admin_notices',array($this,'ca_form_return_admin_notice') );
	}

	function ca_scripts(){
		wp_enqueue_style( 'banner-style', '/wp-content/plugins/' . dirname( plugin_basename(__FILE__)) . '/css/banner.css');
		wp_register_script('banner-script', '/wp-content/plugins/' . dirname( plugin_basename(__FILE__)) . '/js/banner-script.js', array( 'jquery' ));
		// if (!empty(get_option("ca_banner_settings"))) {
		// 	wp_localize_script('banner-script', 'ca_banner_settings', get_option("ca_banner_settings"));
		// }
		wp_enqueue_script('banner-script');
	}

	/**
     * Function output_banner. Outputs the banner html to the page
    */
	function output_banner(){
    	$banner_values = (is_array(get_option('ca_banner_messages'))&&!empty(get_option('ca_banner_messages')))?get_option('ca_banner_messages'):array();
    	$banner_settings = (is_array(get_option('ca_banner_settings'))&&!empty(get_option('ca_banner_settings')))?get_option('ca_banner_settings'):array();
    	$enabled_count = count(array_column($banner_values, "enabled"));
    	if ($enabled_count > 0 && $banner_settings["enabled"]):
			?>
			<div class='ca-banner'>
				<table>
					<tr>
						<?php if ($enabled_count > 1): ?>
							<td>
								<span class="ca-banner-nav">
									<span class="ca-banner-back ca-banner-arrow"> < </span>
									<span><span class="ca-banner-current"> 1 </span> of <span class="ca-banner-total"><?php echo esc_attr($enabled_count);?></span></span>
									<span class="ca-banner-fwd ca-banner-arrow"> > </span>
								</span>
							</td>
						<?php endif; ?>
						<?php $counter = 0; ?>
						<td style="width: 100%">
							<?php foreach ($banner_values as $value) : ?>
								<?php if ($value["enabled"]): ?>
									<span class="ca-banner-message <?php echo ($counter === 0)?'current':''; ?>">
										<span><?php echo esc_attr($value['message']); ?></span>
										<?php if(!empty($value['link'])) echo "<a href='".$value['link']."'>... More</a>" ?>
									</span>
									<?php $counter += 1; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						</td>
						<td><div class="ca-banner-close-container"><span class="ca-banner-close">×</span></div></td>
					</tr>
				</table>
			</div>
			<?php
		endif;
	}

	/**
     * Function ca_register_options_page. Registers an options page
    */
    function ca_register_options_page() {
        add_submenu_page( 
        	'themes.php',
            'Site Banner', //title
            'Site Banner', // menu name
            'manage_options',  // capability
            'site-banner', // slug
            array($this,'ca_output_options_page'), //callback function
            null
        ); 
    }

    /**
     * Function ca_output_options_page. Callback function which outputs the content of the options page
    */
    function ca_output_options_page(){
		wp_enqueue_style('banner-options-style', '/wp-content/plugins/' . dirname( plugin_basename(__FILE__)) . '/css/banner-options.css');
		wp_enqueue_script('banner-options-script', '/wp-content/plugins/' . dirname( plugin_basename(__FILE__)) . '/js/options-script.js', array( 'jquery' ));
    	$table_values = (is_array(get_option('ca_banner_messages'))&&!empty(get_option('ca_banner_messages')))?get_option('ca_banner_messages'):array();
    	$banner_settings = (is_array(get_option('ca_banner_settings'))&&!empty(get_option('ca_banner_settings')))?get_option('ca_banner_settings'):array();
    	?>
    	<div class="wrap">
            <h1>Edit Site Banner</h1>
            <form method="post" action="options.php">
		    <?php settings_fields( 'ca-banner-settings-group' ); ?>
		    <?php do_settings_sections( 'ca-banner-settings-group' ); ?>


		    <h2>Enable Site Banner</h2>
		    <label for="ca_banner_settings[enabled]">Active:</label>
		    <input type="checkbox" name="ca_banner_settings[enabled]" value="true" <?php echo ($banner_settings["enabled"])?"checked":"";?> >


		    <h2>Configure Banner Messages</h2>
		    <table class='ca-banner-messages'>
		    	<thead>
		    		<tr>
		    			<td>No.</td>
		    			<td>Caption</td>
		    			<td>Link (Optional)</td>
		    			<td>Active</td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<?php if(empty($table_values)): ?>
		    			<tr>
			    			<td><span class="ca-row-number">1</span></td>
			    			<td><input type="text" name="ca_banner_messages[0][message]" value=""/></td>
			    			<td><input type="url" name="ca_banner_messages[0][link]"    value=""/></td>
			    			<td><input type="checkbox" name="ca_banner_messages[0][enabled]" value="true"></td>
			    			<td><a href="#" class="ca-delete-row">×</a></td>
			    		</tr>
			    	<?php else: ?>
			    		<?php foreach ($table_values as $key => $value): ?>
			    			<tr>
				    			<td><span class="ca-row-number"><?php echo esc_attr( $key + 1 ); ?></span></td>
				    			<td><input type="text" name="ca_banner_messages[<?php echo esc_attr( $key ); ?>][message]" value="<?php echo esc_attr( $table_values[$key]['message'] ); ?>"/></td>
				    			<td><input type="url" name="ca_banner_messages[<?php echo esc_attr( $key ); ?>][link]"    value="<?php echo esc_attr( $table_values[$key]['link'] ); ?>"/></td>
			    				<td><input type="checkbox" name="ca_banner_messages[<?php echo esc_attr( $key ); ?>][enabled]" value="true" <?php echo ($table_values[$key]["enabled"])?"checked":"";?>></td>
				    			<td><a href="#" class="ca-delete-row">×</a></td>
				    		</tr>
			    		<?php endforeach; ?>
		    		<?php endif; ?>
		    	</tbody>
		    </table>
		    <input type="button" class="ca-add-row" value="Add a Message"/>

		    <h2>General Banner Settings</h2>
		    <label>Cookie Expiry (days):</label>
		    <input type="number" name="ca_banner_settings[cookie_expiry]" min="1">
		    <?php submit_button(); ?>
			</form>
        </div>
        <?php
    }

    /**
     * Function ca_register_settings. Registers options used on the plugin options page
    */
    function ca_register_settings() {
    	register_setting( 'ca-banner-settings-group', 'ca_banner_messages', array('sanitize_callback' => array($this,'sanitize_banner_messages')));
    	register_setting( 'ca-banner-settings-group', 'ca_banner_settings');
    }

    /**
     * Function sanitize_banner_messages. Callback function that sanitizes option values
    */
    function sanitize_banner_messages($inp){
    	$new_array = $inp;
    	foreach ($inp as $key => $value) {
    		if (empty($value["message"])) {
    			unset($new_array[$key]);
    		}
    	}
    	$new_array = array_values($new_array);
    	return $new_array;
    }

}

new Creative_Asset_Site_Banner();

?>