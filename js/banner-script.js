jQuery(function($){

	// retrieve cookie expiry date
	var exdays = 1
	if (typeof ca_banner_settings === "object" && ("cookie_expiry" in ca_banner_settings)) {
		exdays = ca_banner_settings["cookie_expiry"]
	}

	/**
	 * Function setCookie - creates a cookie
	 * @param - exdays - the number of days until the cookie expires
	*/
	function setCookie(exdays) {
		cname = "creative_asset_hide_banner"
		cvalue = true;
	  	var d = new Date();
	  	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	  	var expires = "expires="+ d.toUTCString();
	  	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	/**
	 * Function getCookie - retrieves a cookie
	 * @return - the cookie || ""
	*/
	function getCookie() {
		cname = "creative_asset_hide_banner"
	  	var name = cname + "=";
	  	var decodedCookie = decodeURIComponent(document.cookie);
	  	var ca = decodedCookie.split(';');
	  	for(var i = 0; i <ca.length; i++) {
	    	var c = ca[i];
	    	while (c.charAt(0) == ' ') {
	      		c = c.substring(1);
	    	}
	    	if (c.indexOf(name) == 0) {
	      		return c.substring(name.length, c.length);
	    	}
	  	}
	  	return "";
	}

	// if no cookie is found show the banner
	if (!getCookie()) {
		$( ".ca-banner" ).slideDown( "slow" );
	}
	// $( ".ca-banner" ).slideDown( "slow");
	
	// on banner forward click
	$(".ca-banner-fwd").click(function(){
		// fade out current message
		$( ".ca-banner-message.current" ).fadeOut({
			duration:"slow", 
			start: function(){
				if($(this).next(".ca-banner-message").length){
			    	var num = $(this).next(".ca-banner-message").index() + 1
			    	$(".ca-banner-current").text(num)
			    }
			    else{
			    	$(".ca-banner-current").text(1)
			    }
			},
			complete: function() {
				// remove the current class
				$(this).removeClass("current");
				// find the next message or get the first
			    if($(this).next(".ca-banner-message").length){
			    	var next = $(this).next(".ca-banner-message");
			    }
			    else{
			    	var next = $(this).siblings('.ca-banner-message').first()
			    }
			    // fade in the next message
			    $(next).fadeIn( "slow", function() {
			    	$(this).addClass("current")
			    })
			}
		});
	})

	// on banner previous click
	$(".ca-banner-back").click(function(){
		// fade out current
		$( ".ca-banner-message.current" ).fadeOut({ 
			duration:"slow",
			start: function() {
				if($(this).prev(".ca-banner-message").length){
			    	var num = $(this).prev(".ca-banner-message").index() + 1
			    	$(".ca-banner-current").text(num)
			    }
			    else{
			    	$(".ca-banner-current").text($(".ca-banner-message").length)
			    }
			},
			complete: function() {
				// remove the current class
				$(this).removeClass("current");
				// get previous or last message
			    if($(this).prev(".ca-banner-message").length){
			    	var prev = $(this).prev(".ca-banner-message");
			    }
			    else{
			    	var prev = $(this).siblings('.ca-banner-message').last()
			    }

			    // fade in the previous message
			    $(prev).fadeIn( "slow", function() {
			    	$(this).addClass("current")
			    	$(".ca-banner-current").text($(this).index()+1)
			    })
			}
		});

	})

	// on banner close slide the banner up and set the cookie
	$(".ca-banner-close").click(function(){
		$( ".ca-banner" ).slideUp( "slow", function() {
			setCookie(exdays)
		});
		$("header").animate({ 
	    	'padding-top' : 0,
	  	}, "slow");
	})
})