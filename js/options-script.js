jQuery(function($){

	// on add row click
	$(".ca-add-row").click(function(){
		// clone the first row
		var newrow = $(".ca-banner-messages > tbody > tr:first").clone()
		var rowCount = $('.ca-banner-messages >tbody >tr').length;

		// loop through the inputs
		var inputs = $(newrow).find("input")
        for(var i = 0; i < inputs.length; i++){
        	// change the name
            var name = ($(inputs[i]).attr("name")).replace("0", rowCount)
            // clear the value
            $(inputs[i]).val("")
            $(inputs[i]).attr("name",name)
        }
        // change the row number
        $(newrow).find(".ca-row-number").text(rowCount+1)
        $(".ca-banner-messages > tbody").append(newrow)
    })

    // on delete row click
    $("body").on("click", ".ca-delete-row", function(event){
    	// don't follow link
    	event.preventDefault()

    	// if there is at least one row, remove the current row and update indexes
    	var rowCount = $('.ca-banner-messages >tbody >tr').length
    	if (rowCount > 1) {
    		$(this).parents("tr").remove()
    		updateIndex()
    	}
    })



    /* Drag and drop table functionality */

	var fixHelperModified = function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index) {
		    $(this).width($originals.eq(index).width())
		});
		return $helper;
	},

	updateIndex = function() {
	    $(".ca-banner-messages > tbody > tr").each(function(x, obj){
	    	$(this).find('.ca-row-number').text(x+1)
	    	var inputs = $(obj).find("input")
	    	for(var i = 0; i < inputs.length; i++){
	    		var name = ($(inputs[i]).attr("name")).replace(/\[[0-9]*\]/, "["+x+"]")
	    		$(inputs[i]).attr("name", name)
	    	}
	    });
	};

	$(".ca-banner-messages > tbody").sortable({
	    helper: fixHelperModified,
	    stop: updateIndex
	}).disableSelection();
})